import React from 'react';
import './Home.css';
import backgroundVideo from '../assets/background.mp4';

const Home = () => {
  return (
    <div className="home-container">
      <video autoPlay loop muted className="background-video">
        <source src={backgroundVideo} type="video/mp4" />
        Your browser does not support the video tag.
      </video>
      <div className="home-content">
        <h1>Welcome to Our Website</h1>
        <p>Your journey to excellence begins here.</p>
        <button className="cta-button">Get Started</button>
      </div>
    </div>
  );
};

export default Home;
