import React from 'react';
import { Link } from 'react-router-dom';
import './NevBar.css'; // Adjust the path if necessary
import logo_light from './assets/icon.png.png'
import search_light from './assets/search-w.png'

const Nevbar = () => {
  return (
    <div className='navbar'>
       <img src={logo_light} alt="" className='logo' />
      <ul>
        <li><Link to="/">Home</Link></li>
        <li><Link to="/about">About</Link> </li>
        <li><Link to="/contact">Content</Link></li>
        <li><Link to="/Login">Login</Link></li>
      </ul>

      <div className='search-box'>
        <input type="text"  placeholder='Search'/>
        <img src={search_light} alt=""/>
      </div>

    </div>
  )
};

export default Nevbar;
