import React, { useState } from "react";
import './Login.css'

import user_icon from '../assets/person.png'
import email_icon from '../assets/email.png'
import password_icon from '../assets/password.png'

const Login =  () =>{

    const[action, setAction] = useState("Login");


    return(
        <div className="container">
            <div className="header">
               <div className="Text">{action}</div>
               <div className="underline"></div>
            </div>
<div className="inputs">
    {action==="Login"?<div></div>:<div className="input">
        <img src={user_icon} alt="" />
        <input type = "text" placeholder="NAME" />
    </div>}
   
    <div className="input">
        <img src={email_icon} alt="" />
        <input type = "email" placeholder="EMAIL" />
    </div>
    <div className="input">
        <img src={password_icon} alt="" />
        <input type = "password" placeholder="PASSWORD"/>
    </div>
</div>
<div className="forgot-password">Lost Password <span>Click here</span> </div>
<div className="submit-container">
   <div className={action==="Login"?"submit gray":"submit"} onClick={()=>{setAction("Sign Up")}}>Sign Up</div>
   <div className={action==="Sign Up"?"submit gray":"submit"}  onClick={()=>{setAction("Login")}}>Login</div>
</div>
</div>
    )
}

export default Login;