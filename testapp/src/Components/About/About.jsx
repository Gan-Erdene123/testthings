import React, { useState, useEffect } from 'react';
import './About.css';

import stock1 from '../assets/stock1.jpg';
import stock2 from '../assets/stock2.jpg';
import stock3 from '../assets/stock3.jpg';
import stock4 from '../assets/stock4.jpg';

const images = [stock1, stock2, stock3, stock4];
const About = () => {
  const [currentImageIndex, setCurrentImageIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentImageIndex((prevIndex) => (prevIndex + 1) % images.length);
    }, 7000); // Change image every 3 seconds
    return () => clearInterval(interval);
  }, []);

  const prevImage = () => {
    setCurrentImageIndex((prevIndex) => (prevIndex - 1 + images.length) % images.length);
  };

  const nextImage = () => {
    setCurrentImageIndex((prevIndex) => (prevIndex + 1) % images.length);
  };

  return (
    <div className="about-page">
      <div className="carousel">
        <button className="carousel-button left" onClick={prevImage}>&#9664;</button>
        <img src={images[currentImageIndex]} alt="Showcase" className="carousel-image" />
        <button className="carousel-button right" onClick={nextImage}>&#9654;</button>
      </div>
      <header className="about-header">
        <h1>About Us</h1>
      </header>
      <section className="about-content">
        <p>
          Text afjsafkjalsfjsdf;dlkg; jka;dsgjaskgjlskdjg klasjdgklsdjgkjds;algk ;askgd;kasdg;a;slg lahirfhdfnje hfnkldnflhe kfnj; eio;jFK;siejfigsd; 
        </p>
        <p>
          gkd"SGdgjkDJG  JE;gkld'lkGDjJods KSJdpigjoejsG gSKDgj;ePOGJK SLDKG'jeGd;gmsldgk JKJGDjkgjjdjennmn,n s.dgne;;fsdjG:DSg ;DSGKLSD;gk;sdaohmqpgjmck
        </p>
      </section>
    </div>
  );
}

export default About;

